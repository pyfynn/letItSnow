"""
file: example.py
info: Demo for the letItSnow widget
"""
import sys
import letItSnow
from PySide2 import QtWidgets, QtGui, QtCore


class Window(QtWidgets.QDialog):
    def __init__(self):
        super(Window, self).__init__()
        # Add pseudo GUI
        # --------------------
        # Header
        self.headerWidget = QtWidgets.QWidget()
        headerLayout = QtWidgets.QHBoxLayout(self.headerWidget)
        headerLayout.setContentsMargins(0, 0, 0, 0)
        self.headerLabel = QtWidgets.QLabel("<h2>Comp Loader")
        self.headerLabel.setAlignment(QtCore.Qt.AlignHCenter)
        headerLayout.addWidget(self.headerLabel)
        # Task Picker
        self.taskPickerWidget = QtWidgets.QWidget()
        self.projectLabel = QtWidgets.QLabel("<i>Rainbows & Unicorns")
        self.sequenceCombo = QtWidgets.QComboBox()
        self.shotCombo = QtWidgets.QComboBox()
        self.taskCombo = QtWidgets.QComboBox()
        layout = QtWidgets.QFormLayout(self.taskPickerWidget)
        layout.setContentsMargins(0, 0, 0, 0)
        taskPickerElements = (
            ("project", self.projectLabel),
            ("sequence", self.sequenceCombo),
            ("shot", self.shotCombo),
            ("task", self.taskCombo),
        )
        for label, widget in taskPickerElements:
            widget.setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
            layout.addRow(label, widget)
            layout.labelForField(widget).setEnabled(True)
        # CompTree
        self.compWidget = QtWidgets.QWidget()
        compLayout = QtWidgets.QVBoxLayout(self.compWidget)
        compLayout.setContentsMargins(0, 0, 0, 0)
        self.compLabel = QtWidgets.QLabel("Comps")
        self.compLabel.setAlignment(QtCore.Qt.AlignHCenter)
        self.compTree = QtWidgets.QTreeWidget()
        self.compTree.setHeaderHidden(True)
        compLayout.addWidget(self.compLabel)
        compLayout.addWidget(self.compTree)
        # Picker Widget
        self.pickerWidget = QtWidgets.QWidget()
        hLayout = QtWidgets.QHBoxLayout(self.pickerWidget)
        hLayout.setContentsMargins(0, 0, 0, 0)
        hLayout.addWidget(self.taskPickerWidget)
        hLayout.addWidget(self.compWidget)
        # Footer
        self.footerWidget = QtWidgets.QWidget()
        footerLayout = QtWidgets.QHBoxLayout(self.footerWidget)
        footerLayout.setContentsMargins(0, 0, 0, 0)
        self.loadButton = QtWidgets.QPushButton("Load")
        self.closeButton = QtWidgets.QPushButton("Close")
        footerLayout.addStretch()
        footerLayout.addWidget(self.loadButton)
        footerLayout.addWidget(self.closeButton)
        # Assemble
        mainLayout = QtWidgets.QVBoxLayout(self)
        mainLayout.setContentsMargins(8, 8, 8, 8)
        mainLayout.addWidget(self.headerWidget)
        mainLayout.addWidget(self.pickerWidget)
        mainLayout.addWidget(self.footerWidget)
        # Add Snowy widget last so it draws on top of others
        self.letItSnow = letItSnow.LetItSnow(snowFlakeCount=50, parent=self)
        # EVENTS
        # --------------------
        self.taskCombo.currentTextChanged.connect(self.populateComps)
        # INIT
        # --------------------
        self.populateWidgets()
        self.resize(400, 250)

    def populateWidgets(self):
        # Add some pseudo items to the dropdowns
        self.sequenceCombo.addItems(["sq001", "sq002", "sq003", "sq004"])
        self.shotCombo.addItems(["sh001", "sh002", "sh003", "sh004"])
        self.taskCombo.addItems(["comp", "roto", "rendering"])

    def populateComps(self):
        self.compTree.clear()
        taskName = self.taskCombo.currentText()
        for _ in range(len(taskName)):
            compName = "{taskName}_v{version:03d}".format(taskName=taskName, version=_)
            treeItem = QtWidgets.QTreeWidgetItem(self.compTree)
            treeItem.setText(0, compName)


def main():
    win = Window()
    win.exec_()


if __name__ == "__main__" and not QtWidgets.QApplication.instance():
    app = QtWidgets.QApplication()
    # Make dark-theme
    app.setStyle(QtWidgets.QStyleFactory.create("fusion"))
    darkTheme = QtGui.QPalette()
    darkTheme.setColor(QtGui.QPalette.Base, QtGui.QColor(55, 55, 55))
    darkTheme.setColor(QtGui.QPalette.Window, QtGui.QColor(45, 45, 45))
    darkTheme.setColor(QtGui.QPalette.WindowText, QtGui.QColor(200, 200, 200))
    darkTheme.setColor(QtGui.QPalette.Button, QtGui.QColor(45, 45, 45))
    darkTheme.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(200, 200, 200))
    darkTheme.setColor(QtGui.QPalette.Text, QtGui.QColor(200, 200, 200))
    darkTheme.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(45, 45, 45))
    darkTheme.setColor(QtGui.QPalette.ToolTipBase, QtGui.QColor(200, 200, 200))
    app.setPalette(darkTheme)
    # Show window
    main()
    sys.exit(app.exec_())
else:
    main()
